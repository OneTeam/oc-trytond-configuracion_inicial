#!/bin/bash
set -e

function ayuda () {
    echo "Uso: $0 -d base_datos -b directorio_base -u usuario_postgresql -c archivo_configuracion_trytond"
    echo "El usuario de postgresql debe poder crear base de datos"
    echo "la base_datos no debe existir"
    echo "requiere sistema con python3 instalado"
    echo "requiere sistema con python3 venv module (en debian apt-get install python3-venv)"
    echo "requiere sistema con git instalado"
    echo "requiere sistema con hg instalado"
    echo "requiere sistema con acceso a internet"
}
#validacion de argumentos
tiene_base_dir=0
tiene_base_datos=0
tiene_ps_user=0
tiene_archivo_conf=0

while getopts ":hd:b:u:c:" opt; do
    case ${opt} in
	h )
	    ayuda
	    exit 0
	    ;;
	b )
	    BASE_DIR=$OPTARG
	    if [ ${BASE_DIR: -1} != "/" ]; then
	        BASE_DIR=$BASE_DIR"/"
	    fi
	    tiene_base_dir=1
	    ;;
	d )
	    BASE_DATOS=$OPTARG
	    tiene_base_datos=1
	    ;;
	u )
	    PS_USER=$OPTARG
	    tiene_ps_user=1
	    ;;
	c )
	    ARCHIVO_CONF=$OPTARG
	    tiene_archivo_conf=1
	    ;;
	\? )
	    echo "Opcion invalida: $OPTARG"
	    ayuda
	    exit 1
	    ;;
	: )
	    echo "Opcion invalida: $OPTARG requiere un argumento"
	    ayuda
	    exit 1
	    ;;
    esac
done
shift $((OPTIND -1))
if [ $tiene_base_dir -eq 0 ]; then
    echo "Se requiere base_dir"
    ayuda
    exit 1
fi
if [ $tiene_base_datos -eq 0 ]; then
    echo "Se requiere base_datos"
    ayuda
    exit 1
fi
if [ $tiene_ps_user -eq 0 ]; then
    echo "Se requiere ps_user"
    ayuda
    exit 1
fi
if [ $tiene_archivo_conf -eq 0 ]; then
    echo "Se requiere archivo_conf"
    ayuda
    exit 1
fi

ENV_DIR=$BASE_DIR"env/"
PYTHON_VERSION="3.8"
MODULE_DIR=$ENV_DIR"/lib/python"$PYTHON_VERSION"/site-packages/trytond/modules/"
TRYTOND_VERSION="5.4"
TRYTOND_VERSION_FIX="5.4.1" #version corregida problema postgresql12, concurrencia
SIMPLE_CONF=$(cat <<SCO
[web]
listen = 0.0.0.0:8000
[database]
uri = postgresql://$PS_USER:$PS_USER@127.0.0.1:5432/
SCO
)


echo "creando y activando ambiente"
pushd $BASE_DIR
python3 -mvenv $ENV_DIR
source $ENV_DIR"bin/activate"

echo "crear base de datos"
createdb -U $PS_USER $BASE_DATOS

echo "Ajustando archivo de configuracion"
if [[ $(cat $ARCHIVO_CONF | wc -l ) -eq 0 ]]; then
    echo "$SIMPLE_CONF" > $ARCHIVO_CONF
fi

echo "instalar trytond"
pip install trytond==$TRYTOND_VERSION_FIX
pip install pytz
pip install psycopg2
# necesario para la impresion de facturs
# revisar si aplica en todas las distribuciones
pip install python-magic-debian-bin

echo "instalar modulos oficiales"
pip install trytond-sale-invoice-grouping==$TRYTOND_VERSION trytond-purchase==$TRYTOND_VERSION trytond-sale-price-list==$TRYTOND_VERSION trytond-account_statement==$TRYTOND_VERSION trytond-account_stock_continental==$TRYTOND_VERSION

echo "instalar modulos no oficiales"
pushd $MODULE_DIR

echo "sale_w_tax"
git clone https://git.disroot.org/Etrivial/trytond-sale_w_tax
mv trytond-sale_w_tax sale_w_tax
pushd sale_w_tax
#git update 5.4
popd

echo "sale_pos"
git clone https://git.disroot.org/Etrivial/trytond-sale_pos/
mv trytond-sale_pos sale_pos
pushd sale_pos
#git update 5.4
popd

echo "sale_shop"
git clone https://git.disroot.org/Etrivial/trytond-sale_shop/
mv trytond-sale_shop sale_shop
pushd sale_shop
#git update 5.4
popd

echo "sale_payment"
git clone https://git.disroot.org/Etrivial/trytond-sale_payment/
mv trytond-sale_payment sale_payment
pushd sale_payment
#git update 5.4
popd

echo "on_click_for_sale"
git clone -b $TRYTOND_VERSION https://git.disroot.org/Etrivial/one_click_for_sale/
pushd one_click_for_sale
#git update 5.4
popd

echo "on_click_for_purchase"
git clone -b $TRYTOND_VERSION https://git.disroot.org/Etrivial/one_click_for_purchase/
#pushd one_click_for_sale
#popd

echo "sale_payment_form"
git clone https://git.disroot.org/Etrivial/sale_payment_form/
#pushd sale_payment_form
#git update 5.4
#popd


echo "sale_pos_extras"
git clone https://git.disroot.org/Etrivial/sale_pos_extras/


echo "trytonpsk_account_co_pyme"
git clone https://git.disroot.org/Etrivial/trytonpsk_account_co_pyme/

echo "account_invoice_expenses"
git clone https://git.disroot.org/Etrivial/account_invoice_expenses/

popd
pushd $MODULE_DIR

echo "purchase_discount"
git clone https://github.com/trytonspain/trytond-purchase_discount
mv trytond-purchase_discount purchase_discount
pushd purchase_discount
git checkout 5.4
popd

echo "account_invoice_discount"
git clone https://github.com/trytonspain/trytond-account_invoice_discount/
mv trytond-account_invoice_discount account_invoice_discount
pushd account_invoice_discount
git checkout 5.4
popd

popd
pushd $MODULE_DIR

echo "product_kit"
git clone https://github.com/trytonspain/trytond-product_kit
mv trytond-product_kit product_kit
pushd product_kit
git checkout 5.4
popd

echo "stock_kit"
git clone https://github.com/trytonspain/trytond-stock_kit
mv trytond-stock_kit stock_kit
pushd stock_kit
git checkout 5.4
popd

echo "sale_kit"
git clone https://github.com/trytonspain/trytond-sale_kit
mv trytond-sale_kit sale_kit
pushd sale_kit
git checkout 5.4
popd

popd

echo "Inicialización de base de datos"
trytond-admin -c $ARCHIVO_CONF -d $BASE_DATOS --all -l es

echo "instalar modulo country e importar paises"
pip install pycountry
pip install proteus==$TRYTOND_VERSION
trytond-admin -c $ARCHIVO_CONF -d $BASE_DATOS -u country
python3 $MODULE_DIR"country/scripts/import_countries.py" -c $ARCHIVO_CONF -d $BASE_DATOS

echo "Instalar modulo currency e importar monedas"
pip install forex_python
trytond-admin -c $ARCHIVO_CONF -d $BASE_DATOS -u currency
python3 $MODULE_DIR"currency/scripts/import_currencies.py" -c $ARCHIVO_CONF -d $BASE_DATOS
