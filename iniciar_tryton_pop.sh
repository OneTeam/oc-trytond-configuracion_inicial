#!/bin/bash
set -e

function ayuda () {
    echo "Uso: $0 -d base_datos -b directorio_base -u usuario_postgresql -c archivo_configuracion_trytond"
    echo "El usuario de postgresql debe poder crear base de datos"
    echo "la base_datos no debe existir"
    echo "requiere sistema con python3 instalado"
    echo "requiere sistema con python3 venv module (en debian apt-get install python3-venv)"
    echo "requiere sistema con git instalado"
    echo "requiere sistema con hg instalado"
    echo "requiere sistema con acceso a internet"
    echo "requiere sistema con fossil"
    echo "requiere sistema con xmlsec"
}
#validacion de argumentos
tiene_base_dir=0
tiene_base_datos=0
tiene_ps_user=0
tiene_archivo_conf=0

while getopts ":hd:b:u:c:" opt; do
    case ${opt} in
	h )
	    ayuda
	    exit 0
	    ;;
	b )
	    BASE_DIR=$OPTARG
	    if [ ${BASE_DIR: -1} != "/" ]; then
	        BASE_DIR=$BASE_DIR"/"
	    fi
	    tiene_base_dir=1
	    ;;
	d )
	    BASE_DATOS=$OPTARG
	    tiene_base_datos=1
	    ;;
	u )
	    PS_USER=$OPTARG
	    tiene_ps_user=1
	    ;;
	c )
	    ARCHIVO_CONF=$OPTARG
	    tiene_archivo_conf=1
	    ;;
	\? )
	    echo "Opcion invalida: $OPTARG"
	    ayuda
	    exit 1
	    ;;
	: )
	    echo "Opcion invalida: $OPTARG requiere un argumento"
	    ayuda
	    exit 1
	    ;;
    esac
done
shift $((OPTIND -1))
if [ $tiene_base_dir -eq 0 ]; then
    echo "Se requiere base_dir"
    ayuda
    exit 1
fi
if [ $tiene_base_datos -eq 0 ]; then
    echo "Se requiere base_datos"
    ayuda
    exit 1
fi
if [ $tiene_ps_user -eq 0 ]; then
    echo "Se requiere ps_user"
    ayuda
    exit 1
fi
if [ $tiene_archivo_conf -eq 0 ]; then
    echo "Se requiere archivo_conf"
    ayuda
    exit 1
fi

ENV_DIR=$BASE_DIR"env/"
PYTHON_VERSION="3.9"
MODULE_DIR=$ENV_DIR"/lib/python"$PYTHON_VERSION"/site-packages/trytond/modules/"
TRYTOND_VERSION="6.0"
TRYTOND_VERSION_FIX="6.0" #version corregida problema postgresql12, concurrencia
SIMPLE_CONF=$(cat <<SCO
[web]
listen = 0.0.0.0:8000
[database]
uri = postgresql://$PS_USER:$PS_USER@127.0.0.1:5432/
SCO
)


echo "creando y activando ambiente"
pushd $BASE_DIR
python3 -mvenv $ENV_DIR
source $ENV_DIR"bin/activate"

echo "crear base de datos"
createdb -U $PS_USER $BASE_DATOS

echo "Ajustando archivo de configuracion"
if [[ $(cat $ARCHIVO_CONF | wc -l ) -eq 0 ]]; then
    echo "$SIMPLE_CONF" > $ARCHIVO_CONF
fi

echo "instalar trytond"
pip install trytond==$TRYTOND_VERSION_FIX
pip install pytz
pip install psycopg2
pip install qrcode
# necesario para la impresion de facturs
# revisar si aplica en todas las distribuciones
pip install python-magic-debian-bin

echo "instalar modulos oficiales"
pip install trytond-sale-invoice-grouping==$TRYTOND_VERSION trytond-purchase==$TRYTOND_VERSION trytond-sale-price-list==$TRYTOND_VERSION trytond-account_statement==$TRYTOND_VERSION trytond-account_stock_continental==$TRYTOND_VERSION trytond-analytic_account==$TRYTOND_VERSION trytond-notification_email==$TRYTOND_VERSION trytond-analytic_purchase==$TRYTOND_VERSION trytond-purchase_price_list==$TRYTOND_VERSION


echo "instalar librería Facho"
fossil  clone  https://lib.facho.cyou  libfacho.fossil
fossil open libfacho.fossil --workdir Facho
pushd Facho
python3 setup.py install
popd

echo "instalar modulos no oficiales"
echo "Módulos comunidad españa"
pushd $MODULE_DIR

echo "sale_w_tax"
git clone https://git.disroot.org/Etrivial/trytond-sale_w_tax
mv trytond-sale_w_tax sale_w_tax
pushd sale_w_tax
git checkout $TRYTOND_VERSION
popd

echo "sale_pos"
git clone https://git.disroot.org/Etrivial/trytond-sale_pos/
mv trytond-sale_pos sale_pos
pushd sale_pos
git checkout $TRYTOND_VERSION
popd

echo "sale_shop"
git clone https://git.disroot.org/Etrivial/trytond-sale_shop/
mv trytond-sale_shop sale_shop
pushd sale_shop
git checkout $TRYTOND_VERSION
popd

echo "sale_payment"
git clone https://git.disroot.org/Etrivial/trytond-sale_payment/
mv trytond-sale_payment sale_payment
pushd sale_payment
git checkout $TRYTOND_VERSION
popd

echo "purchase_discount"
git clone https://github.com/trytonspain/trytond-purchase_discount
mv trytond-purchase_discount purchase_discount
pushd purchase_discount
git checkout $TRYTOND_VERSION
popd

echo "account_invoice_discount"
git clone https://github.com/trytonspain/trytond-account_invoice_discount/
mv trytond-account_invoice_discount account_invoice_discount
pushd account_invoice_discount
git checkout $TRYTOND_VERSION
popd


echo "Módulos Presik Colombia"

echo "trytonpsk_account_co_pyme"
git clone https://git.disroot.org/Etrivial/trytonpsk-account_co_pyme/
mv trytonpsk-account_co_pyme account_co_pyme


echo "Módulos Etrivial"

echo "on_click_for_sale"
git clone -b $TRYTOND_VERSION https://git.disroot.org/Etrivial/trytondet-one_click_for_sale/
mv trytondet-one_click_for_sale one_click_for_sale
pushd one_click_for_sale
git checkout $TRYTOND_VERSION
popd

echo "on_click_for_purchase"
git clone -b $TRYTOND_VERSION https://git.disroot.org/Etrivial/trytondet-one_click_for_purchase/
mv trytondet-one_click_for_purchase one_click_for_purchase
pushd one_click_for_sale
git checkout $TRYTOND_VERSION
popd

echo "sale_payment_form"
git clone https://git.disroot.org/Etrivial/trytondet-sale_payment_form/
mv trytondet-sale_payment_form sale_payment_form
pushd sale_payment_form
git checkout $TRYTOND_VERSION
popd

echo "sale_pos_extras"
git clone https://git.disroot.org/Etrivial/trytondet-sale_pos_extras/
mv trytondet-sale_pos_extras sale_pos_extras
pushd sale_pos_extras
git checkout $TRYTOND_VERSION
popd

echo "account_co_co"
git clone https://git.disroot.org/Etrivial/trytondet-account_co_co/
mv trytondet-account_co_co account_co_co
pushd account_co_co
git checkout $TRYTOND_VERSION
popd

echo "account_invoice_subtype"
git clone https://git.disroot.org/Etrivial/trytondet-account_invoice_subtype/
mv trytondet-account_invoice_subtype account_invoice_subtype
pushd account_invoice_subtype
git checkout $TRYTOND_VERSION
popd

echo "analytic_account_co"
git clone https://git.disroot.org/Etrivial/trytondet-analytic_account_co/
mv trytondet-analytic_account_co analytic_account_co
pushd analytic_account_co
git checkout $TRYTOND_VERSION
popd

echo "account_invoice_subtype"
git clone https://git.disroot.org/Etrivial/trytondet-account_co_reports/
mv trytondet-account_co_reports account_co_reports
pushd account_co_reports
git checkout $TRYTOND_VERSION
popd

echo "account_invoice_facho"
git clone https://git.disroot.org/Etrivial/trytondet-account_invoice_facho/
mv trytondet-account_invoice_facho account_invoice_facho
pushd account_invoice_facho
git checkout $TRYTOND_VERSION
popd

echo "purchase_shop"
git clone https://git.disroot.org/Etrivial/trytondet-purchase_shop/
mv trytondet-purchase_shop purchase_shop
#pushd purchase_shop
#git checkout $TRYTOND_VERSION
#popd

echo "purchase_pop"
git clone https://git.disroot.org/Etrivial/trytondet-purchase_pop/
mv trytondet-purchase_pop purchase_pop
#pushd purchase_pop
#git checkout $TRYTOND_VERSION
#popd

echo "purchase_payment"
git clone https://git.disroot.org/Etrivial/trytondet-purchase_payment/
mv trytondet-purchase_payment purchase_payment
#pushd purchase_payment
#git checkout $TRYTOND_VERSION
#popd

echo "purchase_w_tax"
git clone https://git.disroot.org/Etrivial/trytondet-purchase_w_tax/
mv trytondet-purchase_w_tax purchase_w_tax
#pushd purchase_w_tax
#git checkout $TRYTOND_VERSION
#popd

popd

echo "Inicialización de base de datos"
trytond-admin -c $ARCHIVO_CONF -d $BASE_DATOS --all -l es

#No es necesario account_co_co ya lo hace 
#echo "instalar modulo country e importar paises"
pip install pycountry
pip install proteus==$TRYTOND_VERSION
trytond-admin -c $ARCHIVO_CONF -d $BASE_DATOS -u country
#python3 $MODULE_DIR"country/scripts/import_countries.py" -c $ARCHIVO_CONF -d $BASE_DATOS

echo "Instalar modulo currency e importar monedas"
pip install forex_python
trytond-admin -c $ARCHIVO_CONF -d $BASE_DATOS -u currency
python3 $MODULE_DIR"currency/scripts/import_currencies.py" -c $ARCHIVO_CONF -d $BASE_DATOS
