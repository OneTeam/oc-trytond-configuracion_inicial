# iniciar_tryton

Crea una ambiente de python e instala trytond con los módulos usados en escenarios comunes.

## iniciar_tryton
[iniciar_tryton.org](./iniciar_tryton.org "iniciar_tryton.org") creado con emacs-org es quien genera los otros 2 archivos:
* [iniciar_tryton.sh](iniciar_tryton.sh "iniciar_tryton.sh"): se genera en emacs con *M-x org-babel-tangle*
* [iniciar_tryton.md](iniciar_tryton.md "iniciar_tryton.md"): se genera en emacs con *M-x org-md-export-to-markdown*
